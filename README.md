# Lazarus Campaign

Storing XSLT-rendered **Fate Condensed** PCs for the _Lazarus_ campaign.

[Google Drive folder](https://drive.google.com/drive/folders/1hcFsGnNMH1_035RANrMfWDLpkvoK56du)

## PCs

- [Bridget](out/FateCore/Bridget.pdf) (Jenn)
- [Darius Wainwright](out/FateCore/Darius Wainwright.pdf) (Kevin)
- [Doc Clock](out/FateCore/Dr. Cornelius.pdf) (Paul)
- [Meir Hemingway](out/FateCore/Meir Hemingway.pdf) (Mo)
- [Persephina](out/FateCore/Persephina.pdf) (Brian)
- [Susan Winters](out/FateCore/Susan Winters.pdf) (Phil)

